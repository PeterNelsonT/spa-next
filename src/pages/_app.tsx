/* eslint-disable react-hooks/exhaustive-deps */
import "antd/dist/antd.css";
import type { AppProps } from "next/app";
import { useEffect, useState } from "react";

// renders child components ONLY if running client-side
function ClientSideRendering({ children }: any) {
  const [csrReady, setCsrReady] = useState(false);

  // NOTE: this will NEVER fire on Server Side, but it will (of course) run in the Browser
  useEffect(() => {
    setCsrReady(true);
  }, []);

  return csrReady ? children : null;
}

// Intended to ONLY run client-side (as a SPA - Single Page Application) in React 18+ / NextJS 12+
// ala "SPA Mode"
function _app({ Component, pageProps }: AppProps) {
  return (
    <ClientSideRendering>
      <Component {...pageProps} />
    </ClientSideRendering>
  );
}

export default _app;
